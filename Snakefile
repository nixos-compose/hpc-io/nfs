# PATH_HERE="/home/quentin/ghq/gitlab.inria.fr/qguillot/eval-folding-pfs"
PATH_HERE="/home/qguilloteau/eval-folding-pfs"
RESULT_FOLDER=f"{PATH_HERE}/data"

NIX_COMPANION_FILE=f"{PATH_HERE}/nix-user-chroot.sh"

FLAVOURS = [

    # "g5k-nfs-store",
    # "g5k-ramdisk"
    "g5k-image"

    # "vm-ramdisk"
]

NB_NODES = [
    #5,
  9, 17, 25
 # 13,
 #17, 21, 33 , 65
#, 25
]

BLOCK_SIZES = [
"1M",
"10M",
"100M",
"1G" 
]


rule all:
    input:
        # expand(["nxc/build/composition::{file}"], file=FLAVOURS),
        # expand(["{result_folder}/csv_zip/results_csv_ior_{nb_nodes}_nodes_{flavour}.zip"], result_folder=RESULT_FOLDER, flavour=FLAVOURS, nb_nodes=NB_NODES),
        expand(["{path_here}/data/json_zip/results_ior_{nb_nodes}_nodes_{block_size}_block_size_{flavour}.zip"], path_here=PATH_HERE, flavour=FLAVOURS, nb_nodes=NB_NODES, block_size=BLOCK_SIZES),
        # expand(["{result_folder}/csv/{nb_nodes}_{flavour}.csv"], result_folder=RESULT_FOLDER, flavour=FLAVOURS, nb_nodes=NB_NODES)
        # expand(["data/repeat/iter_{iter}/csv_zip/results_csv_ior_{nb_nodes}_nodes_{flavour}.zip"], iter=[1, 2, 3, 4, 5], nb_nodes=8, flavour=FLAVOURS),
        # expand(["data/repeat/iter_{iter}/csv/{nb_nodes}_{flavour}.csv"], iter=[1, 2, 3, 4, 5], nb_nodes=8, flavour=FLAVOURS),
        # "Rplots.pdf",
        # "analysis/rmd/main.pdf",
        # "analysis/repeat.pdf"
        # expand(["{result_folder}/results_ior_{nb_nodes}_{flavour}.csv"], result_folder=RESULT_FOLDER, flavour=FLAVOURS, nb_nodes=NB_NODES),

rule build_nxc_image:
    input:
        "nxc/flake.nix",
        "nxc/flake.lock",
        "nxc/composition.nix",
        "nxc/my_scripts.nix",
        "nxc/script_ior.nix"
    output:
        "nxc/build/composition::{flavour}"
    shell:
        "cd nxc; nix develop --command nxc build -f {wildcards.flavour}"

rule run_ior:
    input:
        "nxc/build/composition::{flavour}"
    output:
        "{PATH_HERE}/data/json_zip/results_ior_{nb_nodes}_nodes_{block_size}_block_size_{flavour}.zip"
    shell:
        "cd nxc; nix develop --command python3 script.py --nxc_build_file {PATH_HERE}/{input} --nb_nodes {wildcards.nb_nodes} --block_size {wildcards.block_size} --result_dir {RESULT_FOLDER} --flavour {wildcards.flavour} --outfile {output} --walltime 5"

rule json_to_csv:
    input:
        "analysis/ior_json_to_csv.py",
        "{RESULT_FOLDER}/results_ior_{nb_nodes}_nodes_{flavour}.zip"
    output:
        "{RESULT_FOLDER}/csv_zip/results_csv_ior_{nb_nodes}_nodes_{flavour}.zip"
    shell:
        "nix develop .#python --command python3 {input} {output}"

rule generate_csv:
    input:
        "analysis/zip_to_csv.R",
        "data/csv_zip/results_csv_ior_{nb_nodes}_nodes_{flavour}.zip"
    output:
        "data/csv/{nb_nodes}_{flavour}.csv"
    shell:
        "nix develop .#rshell --command Rscript {input} {output}"

rule generate_repeat_csv:
    input:
        "analysis/zip_to_csv.R",
        "data/repeat/iter_{iter}/csv_zip/results_csv_ior_{nb_nodes}_nodes_{flavour}.zip"
    output:
        "data/repeat/iter_{iter}/csv/{nb_nodes}_{flavour}.csv"
    shell:
        "nix develop .#rshell --command Rscript {input} {output}"

rule repeat_analysis:
    input:
        "analysis/repeat.R",
        expand(["data/repeat/iter_{iter}/csv/{nb_nodes}_{flavour}.csv"], iter=[1, 2, 3, 4, 5], nb_nodes=8, flavour=FLAVOURS)
    output:
        "analysis/repeat.pdf"
    shell:
        "nix develop .#rshell --command Rscript {input} {output}"
    

rule general_analysis:
    input:
        "analysis/analysis.R",
        expand(["{result_folder}/csv/{nb_nodes}_{flavour}.csv"], result_folder=RESULT_FOLDER, flavour=FLAVOURS, nb_nodes=NB_NODES)
    output:
        "Rplots.pdf"
    shell:
        "nix develop .#rshell --command Rscript {input}"

rule paper:
    input:
        "analysis/rmd/main.Rmd",
        expand(["{result_folder}/csv/{nb_nodes}_{flavour}.csv"], result_folder=RESULT_FOLDER, flavour=FLAVOURS, nb_nodes=NB_NODES)
    output:
        "analysis/rmd/main.pdf"
    shell:
        "nix develop .#rmdshell --command Rscript -e 'rmarkdown::render(\"analysis/rmd/main.Rmd\", \"pdf_document\")'"



# rule r_analysis:
#     input:
#         "analysis/script.R",
#         expand(["{result_folder}/results_ior_{nb_nodes}_{flavour}.csv"], result_folder=RESULT_FOLDER, flavour=FLAVOURS, nb_nodes=NB_NODES)
#     output:
#         "analysis/Rplots.pdf"
#     shell:
#         "cd analysis; nix develop .#rshell --command Rscript script.R"
