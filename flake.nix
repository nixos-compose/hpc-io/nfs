{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.11";
  };

  outputs = { self, nixpkgs }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };

    rPkgs = with pkgs.rPackages; [
      tidyverse
    ];

    myR = pkgs.rWrapper.override { packages = rPkgs; };

    rmdPkgs = with pkgs.rPackages; [
        rmarkdown
        markdown
        knitr
        tinytex
        magick
        codetools
      ];

      myRmd = pkgs.rWrapper.override { packages = rPkgs ++ rmdPkgs; };
      myRstudio = pkgs.rstudioWrapper.override { packages = rPkgs ++ rmdPkgs; };
  in
  {
    devShells.${system} = {
      default = pkgs.mkShell {
        buildInputs = [ pkgs.graphviz pkgs.snakemake ];
      };

      python = pkgs.mkShell {
        buildInputs = [
          pkgs.python3
        ];
      };

      rshell = pkgs.mkShell {
        buildInputs = [ myR ];
      };

      rmdshell = pkgs.mkShell {
        buildInputs = with pkgs; [ myRmd pandoc texlive.combined.scheme-full ];
      };

      rmddev = pkgs.mkShell {
        buildInputs = with pkgs; [ myRstudio pandoc texlive.combined.scheme-full ];
      };

    };
  };
}

