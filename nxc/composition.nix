{ pkgs, setup, ... }: {

  nodes =
    let
      scripts = import ./my_scripts.nix { inherit pkgs setup; };
      common_config = { pkgs, ... }:
        {
          environment.etc = {
            ior_script = {
              text = import ./script_ior.nix { inherit pkgs setup; };
            };
          };
          networking.firewall.enable = false;
        };
    in
    {
      node = { pkgs, ... }:
        {
          imports = [ common_config ];

          environment.systemPackages = with pkgs; [ ior openmpi scripts.start_ior_nodes scripts.generate_ior_config ];

          fileSystems."/data" = {
            device = "server:/";
            fsType = "nfs";
          };
        };

      server = { pkgs, ... }:
        {
          imports = [ common_config ];

          # Enable the nfs server services
          services.nfs.server.enable = true;

          # Define a mount point at /srv/shared
          services.nfs.server.exports = ''
            /srv/shared *(rw,no_subtree_check,fsid=0,no_root_squash)
          '';
          services.nfs.server.createMountPoints = true;
          services.nfs.server.nproc = setup.params.nfsNbProcs;
        };
    };
  testScript = ''
  '';
}
