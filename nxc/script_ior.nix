{ pkgs, setup, ... }:
let
    numTasks = setup.params.numTasks;
    nfsMountPoint = setup.params.nfsMountPoint;
in
''
IOR START
    api=POSIX
    testFile=testFile
    hintsFileName=hintsFile
    multiFile=0
    interTestDelay=5
    readFile=0
    writeFile=1
    filePerProc=1
    checkWrite=1
    checkRead=0
    keepFile=1
    quitOnError=0
    outlierThreshold=0
    setAlignment=1
    singleXferAttempt=0
    individualDataSets=0
    verbose=0
    collective=0
    preallocate=0
    useFileView=0
    keepFileWithError=0
    setTimeStampSignature=1
    useSharedFilePointer=0
    useStridedDatatype=0
    uniqueDir=0
    fsync=0
    storeFileOffset=0
    maxTimeDuration=60
    deadlineForStonewalling=0
    useExistingTestFile=0
    useO_DIRECT=0
    showHints=0

    repetitions=5
    numTasks=${builtins.toString numTasks}
    segmentCount=1
    blockSize=1G
    transferSize=4M

    summaryFile=/data/results_ior.json
    summaryFormat=JSON
RUN
    writeFile=0
    readFile=1
    checkWrite=0
    checkRead=1
RUN
IOR STOP
''
